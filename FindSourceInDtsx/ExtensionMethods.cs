﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace FindSourceInDtsx
{
    public static class ExtensionMethods
    {
        public static bool ConvertToBool(bool? b)
        {
            if (b != null && b != false)
            {
                return true;
            }
            return false;
        }
        public static string ConvertToString(string? st)
        {
            if (st != null)
            {
                return Convert.ToString(st);
            }
            return "";
        }
    }
}
