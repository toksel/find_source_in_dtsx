﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace FindSourceInDtsx
{
    public class ResultSet : IComparer<ResultSet>, IEquatable<ResultSet>
    {
        public string DestinationTable { get;  }
        public string SourceTable { get;  }
        public string PathDTSX { get; }
        public string SourceVariable { get; }
        public string SourceLink { get; }
        public string SourceDB { get; }
        public string SQLCommand { get; }
        public int CountNonEmptyField { get; }

        public ResultSet(string destinationTable, string? sourceTable, string pathDTSX, string? sourceVariable, string? sourceLink, string? sourceDB, string? SQLCommand)
        {
            DestinationTable = destinationTable;
            SourceTable = sourceTable ?? "";
            PathDTSX = pathDTSX;
            SourceVariable = sourceVariable ?? "";
            SourceLink = sourceLink ?? "";
            SourceDB = sourceDB ?? "";
            this.SQLCommand = SQLCommand ?? "";
            var nonEmpty = new List<string>
            {
                destinationTable,
                pathDTSX

            };
            if (!string.IsNullOrEmpty(sourceTable))
            {
                nonEmpty.Add(sourceTable);
            }
            if (!string.IsNullOrEmpty(sourceVariable))
            {
                nonEmpty.Add(sourceVariable);
            }
            if (!string.IsNullOrEmpty(sourceLink))
            {
                nonEmpty.Add(sourceLink);
            }
            if (!string.IsNullOrEmpty(sourceDB))
            {
                nonEmpty.Add(sourceDB);
            }
            if (!string.IsNullOrEmpty(SQLCommand))
            {
                nonEmpty.Add(SQLCommand);
            }

            CountNonEmptyField = nonEmpty.Count;          
        }

        public override string ToString()
        {
            string result = "";
            result += "Destination table:\n";
            result += DestinationTable + "\n\n";
            result += "Package path:\n";
            result += PathDTSX + "\n\n";
            if (SourceTable != "")
            {
                result += "SourceText table:\n";
                result += SourceTable + "\n\n";
            }
            if (SourceVariable != "")
            {
                result += "SourceSSIS variable:\n";
                result += SourceVariable + "\n\n";
            }
            if (SourceLink != "")
            {
                result += "SourceLink:\n";
                result += SourceLink + "\n\n";
            }
            if (SourceDB != "")
            {
                result += "SourceDataBase:\n";
                result += SourceDB + "\n\n";
            }
            if (SQLCommand != "")
            {
                result += "SQL:\n";
                result += SQLCommand + "\n";
            }

            result += "----------------------------------------------------------------\n\n";

            return result;
        }

        public int Compare([AllowNull] ResultSet x, [AllowNull] ResultSet y)
        {
            if (x.CountNonEmptyField > y.CountNonEmptyField)
            {
                return 1;
            }
            else if (x.CountNonEmptyField == y.CountNonEmptyField)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        public bool Equals([AllowNull] ResultSet other)
        {
            bool result = false;

            if (this.DestinationTable.Equals(other.DestinationTable) 
                && this.SourceTable.Equals(other.SourceTable)
                && this.PathDTSX.Equals(other.PathDTSX)
                && this.SourceVariable.Equals(other.SourceVariable)
                && this.SourceLink.Equals(other.SourceLink)
                && this.SourceDB.Equals(other.SourceDB)
                && this.SQLCommand.Equals(other.SQLCommand))
            {
                result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            int hashDestinationTable = ExtensionMethods.ConvertToString(DestinationTable).GetHashCode();
            int hashSourceTable = ExtensionMethods.ConvertToString(SourceTable).GetHashCode();
            int hashPathDTSX = ExtensionMethods.ConvertToString(PathDTSX).GetHashCode(); 
            int hashSourceVariable = ExtensionMethods.ConvertToString(SourceVariable).GetHashCode();
            int hashSourceLink = ExtensionMethods.ConvertToString(SourceLink).GetHashCode();
            int hashSourceDB = ExtensionMethods.ConvertToString(SourceDB).GetHashCode();
            int hashSQLCommand = ExtensionMethods.ConvertToString(SQLCommand).GetHashCode();
            return hashDestinationTable + hashSourceTable + hashPathDTSX + hashSourceVariable + hashSourceLink + hashSourceDB + hashSQLCommand;
        }
    }

}
