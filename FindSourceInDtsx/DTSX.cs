﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace FindSourceInDtsx
{
    public class DTSX
    {
        public string FullPath { get; set; }
        public string Pattern { get; set; }

        public DTSX(string fullPath)
        {
            FullPath = fullPath;
        }
        public DTSX(string fullPath, string pattern)
        {
            FullPath = fullPath;
            Pattern = pattern;
        }


        public string ReadContent()
        {
            string result = File.ReadAllText(FullPath);
            return result;
        }
        public bool ContainsPattern()
        {
            if (ReadContent().ToLower().Contains(Pattern.ToLower()))
            {
                return true;
            }
            return false;
        }
        public string[] GetContentLines()
        {
            return ReadContent().Split(new char[] { '\n' });
        }

        private string CropLine(string line, char startChar, char endChar, int modeIndexOf)
        {
            if (modeIndexOf == 0)
            {
                if (line.Contains(startChar))
                {
                    line = line.Substring(line.IndexOf(startChar) + 1, line.Length - line.IndexOf(startChar) - 1);
                }               
            }
            else
            {
                if (line.Contains(startChar))
                {
                    line = line.Substring(line.LastIndexOf(startChar) + 1, line.Length - line.LastIndexOf(startChar) - 1);
                }                  
            }

            if (line.Contains(endChar))
            {
                line = line.Substring(0, line.IndexOf(endChar));
            }              
            line = line.Trim();
            return line;
        }

        private int GetLineNumberFromArray(string[] lines, string pattern, int start, int end, int modeBreak)
        {
            int lineNumber = 0;
            for (int i = start; i < end; i++)
            {
                if (lines[i].Contains(pattern))
                {
                    lineNumber = i;
                    if (modeBreak == 0)
                    {
                        break;
                    }                 
                }
            }
            return lineNumber;
        }

        #region GetSourceTable() v1
        //public string GetSourceTable()
        //{
        //    if (ContainsPattern())
        //    {
        //        int rowContainsPattern = 0;
        //        string[] lines = GetContentLines();
        //        for (int i = 0; i < lines.Length; i++)
        //        {
        //            if (lines[i].ToLower().Contains(Pattern.ToLower()))
        //            {
        //                rowContainsPattern = i;                        
        //                break;
        //            }
        //        }
        //        int rowDTSExecutableStart = GetLineNumberFromArray(lines, "<DTS:Executable", 0, rowContainsPattern, 1);
        //        int rowDTSExecutableEnd = GetLineNumberFromArray(lines, "</DTS:Executable>", rowContainsPattern, lines.Length, 1);
        //        int rowContainsSourceText = GetLineNumberFromArray(lines, "<DTS:Executables>", rowDTSExecutableStart, rowDTSExecutableEnd, 0);
        //        int rowContainsSourceDBvariable = GetLineNumberFromArray(lines, "\"DB_Name\"", rowDTSExecutableStart, rowDTSExecutableEnd, 0);
        //        int rowContainsSourceLinkvariable = GetLineNumberFromArray(lines, "\"Link_Name\"", rowDTSExecutableStart, rowDTSExecutableEnd, 0);
        //        int rowSQLStatement = GetLineNumberFromArray(lines, "SqlStatementSource", rowDTSExecutableStart, rowDTSExecutableEnd, 0);

        //        string sourceTable = lines[rowContainsPattern - 3];
        //        sourceTable = CropLine(sourceTable, '"', '"', 0);

        //        string sourceText = lines[rowContainsSourceText + 2];
        //        sourceText = CropLine(sourceText, '"', '"', 0);

        //        string sSql = lines[rowSQLStatement];
        //        sSql = CropLine(sSql, '"', '"', 0);             

        //        string sourceDBvariable = lines[rowContainsSourceDBvariable + 1];
        //        sourceDBvariable = CropLine(sourceDBvariable, ':', '"', 1);

        //        string sourceLinkvariable = lines[rowContainsSourceLinkvariable + 1];
        //        sourceLinkvariable = CropLine(sourceLinkvariable, ':', '"', 1);

        //        int rowContainsSourceDB = GetLineNumberFromArray(lines, sourceDBvariable, 0, lines.Length, 0);
        //        int rowContainsSourceLink = GetLineNumberFromArray(lines, sourceLinkvariable, 0, lines.Length, 0);

        //        string sourceDB = lines[rowContainsSourceDB + 2];
        //        sourceDB = CropLine(sourceDB, '>', '<', 0);

        //        string sourceLink = lines[rowContainsSourceLink + 2];
        //        sourceLink = CropLine(sourceLink, '>', '<', 0);

        //        sSql = sSql.Replace("&#xA;","\n");
        //        sSql = sSql.Replace("@Batch", "-69");
        //        sSql = sSql.Replace("@Link_Name", "'" + sourceLink + "'");
        //        sSql = sSql.Replace("@DB_Name", "'" + sourceDB + "'");
        //        sSql = sSql.Replace("@TableNameSource", "'" + sourceTable + "'");
        //        sSql = sSql.Replace("@TableNameDestination", "'" + Pattern + "'");

        //        string result = "Destination table:\n";
        //        result += Pattern + "\n\n";
        //        result += "Package path:\n";
        //        result += FullPath + "\n\n";
        //        result += "SourceText table:\n";
        //        result += sourceTable + "\n\n";               
        //        result += "SourceText:\n";
        //        result += sourceText + "\n\n";
        //        result += "SourceLink:\n";
        //        result += sourceLink + "\n\n";
        //        result += "SourceDataBase:\n";
        //        result += sourceDB + "\n\n";
        //        result += "Procedure:\n";
        //        result += sSql + "\n\n\n";

        //        return result;
        //    }
        //    return null;
        //}
        #endregion

        private XElement getParentElementUntilCertain(XElement initElement, XName elementName)
        {
            if (initElement == null)
            {
                return null;
            }
            while (initElement?.Name != elementName)
            {
                initElement = initElement.Parent;
                if (initElement == null)
                {                    
                    return null;
                }
            }
            return initElement;             
        }

        private XElement getNextElementUntilCertain(XElement initElement, XName elementName)
        {
            if (initElement == null)
            {
                return null;
            }
            while (initElement?.Name != elementName)
            {
                initElement = initElement.NextNode as XElement;
                if (initElement == null)
                {
                    return null;
                }
            }
            return initElement;
        }

        private XElement getChildElementUntilCertain(XElement initElement, XName attributeName, string attributeValue)
        {
            while (!ExtensionMethods.ConvertToBool(initElement?.Attribute(attributeName)?.Value.Contains(attributeValue)))          
            {
                if (initElement == null)
                {
                    return null;
                }
                if (initElement.HasElements)
                {
                    var childElements = initElement?.Elements();
                    foreach (var elem in childElements)
                    {
                        initElement = getChildElementUntilCertain(elem, attributeName, attributeValue);
                        if (elem == null)
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            return initElement;
        }

        private XElement getChildElementUntilCertain(XElement initElement, XName elementName)
        {
            if (initElement == null)
            {
                return null;
            }
            if (initElement.HasElements)
            {
                while (initElement?.Name != elementName)
                {
                    if (initElement == null)
                    {
                        return null;
                    }
                    if (initElement.HasElements)
                    {
                        var childElements = initElement.Elements();
                        foreach (var elem in childElements)
                        {
                            initElement = getChildElementUntilCertain(elem, elementName);
                            if (elem == null)
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
            
            return initElement;
        }

        private string ReturnListValueCertainIndex (List<string> list, int n)
        {
            if (list.Count >= n + 1)
            {
                return list[n];
            }
            return "";
        }

        #region GetSourceTable returns string
        //public string GetSourceTable()
        //{
        //    if (ContainsPattern())
        //    {
        //        string result = "";

        //        try
        //        {
        //            var doc = XDocument.Load(FullPath).Root;

        //            XNamespace dts = "www.microsoft.com/SqlServer/Dts";
        //            XNamespace sql = "www.microsoft.com/sqlserver/dts/tasks/sqltask";

        //            XElement elemPattern = doc?.Descendants()?.Where(x => x.Attribute("Value")?.Value == Pattern).FirstOrDefault();
        //            XElement elemSource = elemPattern?.PreviousNode as XElement;
        //            XElement elemIncludedColumns = elemPattern?.NextNode as XElement;
        //            XElement elemExcludedColumns = elemIncludedColumns?.NextNode as XElement;
        //            XElement elemDistinct = elemExcludedColumns?.NextNode as XElement;
        //            XElement elemWhere = elemDistinct?.NextNode as XElement;
        //            XElement elemPatternParent = getParentElementUntilCertain(elemPattern, dts + "ForEachEnumerator");
        //            XElement elemPatternParentNext = getNextElementUntilCertain(elemPatternParent, dts + "ForEachVariableMappings");
        //            XElement elemContainsSourceVariable = getChildElementUntilCertain(elemPatternParentNext, dts + "VariableName", "Source");

        //            XElement elemPatternParentNextSql = getNextElementUntilCertain(elemPatternParent, dts + "Executables");
        //            XElement elemPatternParentNextSqlChild = getChildElementUntilCertain(elemPatternParentNextSql, dts + "ObjectData");
        //            XElement elemPatternParentNextSqlExec = elemPatternParentNextSqlChild?.Element(sql + "SqlTaskData");

        //            string destinationTable = elemPattern?.Attribute("Value")?.Value;
        //            string sourceTable = elemSource?.Attribute("Value")?.Value;
        //            string includedColumns = elemIncludedColumns?.Attribute("Value")?.Value ?? "";
        //            string excludedColumns = elemExcludedColumns?.Attribute("Value")?.Value ?? "";
        //            string distinct = elemDistinct?.Attribute("Value")?.Value ?? "";
        //            string where = elemWhere?.Attribute("Value")?.Value ?? "";


        //            string sourceVariableFull = elemContainsSourceVariable?.Attribute(dts + "VariableName")?.Value;
        //            string sourceVariable = sourceVariableFull?.Substring(sourceVariableFull.IndexOf(':') + 2);
        //            sourceVariable = sourceVariable?.Substring(0, sourceVariable.IndexOf('_'));

        //            XElement elemSourceDatabase = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_DB_Name").FirstOrDefault();
        //            XElement elemSourceLinkedSever = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_Link_Name").FirstOrDefault();

        //            string sourceDB = elemSourceDatabase?.Value;
        //            string sourceLink = elemSourceLinkedSever?.Value;

        //            string sqlCommandFull = elemPatternParentNextSqlExec?.Attribute(sql + "SqlStatementSource")?.Value;
        //            if (sqlCommandFull != null)
        //            {
        //                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
        //                sqlCommandFull = sqlCommandFull.Replace("@Link_Name", "'" + sourceLink + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@DB_Name", "'" + sourceDB + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableNameSource", "'" + sourceTable + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableNameDestination", "'" + destinationTable + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableIncludedColumns", "'" + includedColumns + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableExcludedColumns", "'" + excludedColumns + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableDistinct", distinct);
        //                sqlCommandFull = sqlCommandFull.Replace("@TableWhere", "'" + where + "'");
        //            }


        //            result += "Destination table:\n";
        //            result += destinationTable + "\n\n";
        //            result += "Package path:\n";
        //            result += FullPath + "\n\n";
        //            result += "SourceText table:\n";
        //            result += sourceTable + "\n\n";
        //            result += "SourceSSIS variable:\n";
        //            result += sourceVariable + "\n\n";
        //            result += "SourceLink:\n";
        //            result += sourceLink + "\n\n";
        //            result += "SourceDataBase:\n";
        //            result += sourceDB + "\n\n";
        //            result += "SQL:\n";
        //            result += sqlCommandFull + "\n\n";
        //            return result;
        //        }
        //        catch (Exception ex)
        //        {
        //            result += ex.Message + "\n";
        //            return result;
        //        }

        //    }
        //    else
        //    {
        //        return null;
        //    }

        //}
        #endregion


        #region GetSourceTable returns ResultSet v1
        //public ResultSet GetSourceTable()
        //{
        //    if (ContainsPattern())
        //    {
        //        try
        //        {
        //            var doc = XDocument.Load(FullPath).Root;

        //            XNamespace dts = "www.microsoft.com/SqlServer/Dts";
        //            XNamespace sql = "www.microsoft.com/sqlserver/dts/tasks/sqltask";

        //            XElement elemPattern = doc?.Descendants()?.Where(x => x.Attribute("Value")?.Value == Pattern).FirstOrDefault();
        //            XElement elemSource = elemPattern?.PreviousNode as XElement;
        //            XElement elemIncludedColumns = elemPattern?.NextNode as XElement;
        //            XElement elemExcludedColumns = elemIncludedColumns?.NextNode as XElement;
        //            XElement elemDistinct = elemExcludedColumns?.NextNode as XElement;
        //            XElement elemWhere = elemDistinct?.NextNode as XElement;
        //            XElement elemPatternParent = getParentElementUntilCertain(elemPattern, dts + "ForEachEnumerator");
        //            XElement elemPatternParentNext = getNextElementUntilCertain(elemPatternParent, dts + "ForEachVariableMappings");
        //            XElement elemContainsSourceVariable = getChildElementUntilCertain(elemPatternParentNext, dts + "VariableName", "Source");

        //            XElement elemPatternParentNextSql = getNextElementUntilCertain(elemPatternParent, dts + "Executables");
        //            XElement elemPatternParentNextSqlChild = getChildElementUntilCertain(elemPatternParentNextSql, dts + "ObjectData");
        //            XElement elemPatternParentNextSqlExec = elemPatternParentNextSqlChild?.Element(sql + "SqlTaskData");

        //            string destinationTable = elemPattern?.Attribute("Value")?.Value;
        //            string sourceTable = elemSource?.Attribute("Value")?.Value;
        //            string includedColumns = elemIncludedColumns?.Attribute("Value")?.Value ?? "";
        //            string excludedColumns = elemExcludedColumns?.Attribute("Value")?.Value ?? "";
        //            string distinct = elemDistinct?.Attribute("Value")?.Value ?? "";
        //            string where = elemWhere?.Attribute("Value")?.Value ?? "";


        //            string sourceVariableFull = elemContainsSourceVariable?.Attribute(dts + "VariableName")?.Value;
        //            string sourceVariable = sourceVariableFull?.Substring(sourceVariableFull.IndexOf(':') + 2);
        //            sourceVariable = sourceVariable?.Substring(0, sourceVariable.IndexOf('_'));

        //            XElement elemSourceDatabase = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_DB_Name").FirstOrDefault();
        //            XElement elemSourceLinkedSever = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_Link_Name").FirstOrDefault();

        //            string sourceDB = elemSourceDatabase?.Value;
        //            string sourceLink = elemSourceLinkedSever?.Value;

        //            string sqlCommandFull = elemPatternParentNextSqlExec?.Attribute(sql + "SqlStatementSource")?.Value;
        //            if (sqlCommandFull != null)
        //            {
        //                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
        //                sqlCommandFull = sqlCommandFull.Replace("@Link_Name", "'" + sourceLink + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@DB_Name", "'" + sourceDB + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableNameSource", "'" + sourceTable + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableNameDestination", "'" + destinationTable + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableIncludedColumns", "'" + includedColumns + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableExcludedColumns", "'" + excludedColumns + "'");
        //                sqlCommandFull = sqlCommandFull.Replace("@TableDistinct", distinct);
        //                sqlCommandFull = sqlCommandFull.Replace("@TableWhere", "'" + where + "'");
        //            }

        //            return new ResultSet(destinationTable, sourceTable ?? Pattern, FullPath, sourceVariable, sourceLink, sourceDB, sqlCommandFull);

        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message);
        //            return null;
        //        }

        //    }
        //    else
        //    {
        //        return null;
        //    }

        //}
        #endregion

        public IEnumerable<ResultSet> GetSourceTable()
        {
            if (ContainsPattern())
            {
                try
                {
                    var doc = XDocument.Load(FullPath).Root;

                    XNamespace dts = "www.microsoft.com/SqlServer/Dts";
                    XNamespace sql = "www.microsoft.com/sqlserver/dts/tasks/sqltask";

                    List<ResultSet> results = new List<ResultSet>();

                    var elemPatterns = doc?.Descendants()?.Where(x => ExtensionMethods.ConvertToBool(x.Attribute("Value")?.Value.ToLower().Contains(Pattern.ToLower())));
                    foreach (var elemPattern in elemPatterns)
                    {

                        var fileItems = elemPattern.Parent.Elements();

                        List<string> parameters = new List<string>();
                        foreach (var fileItemValue in fileItems)
                        {
                            parameters.Add(fileItemValue?.Attribute("Value")?.Value);
                        }

                        XElement elemPatternParent = getParentElementUntilCertain(elemPattern, dts + "ForEachEnumerator");
                        XElement elemPatternParentNext = getNextElementUntilCertain(elemPatternParent, dts + "ForEachVariableMappings");
                        XElement elemContainsSourceVariable = getChildElementUntilCertain(elemPatternParentNext, dts + "VariableName", "Source");

                        XElement elemPatternParentNextSql = getNextElementUntilCertain(elemPatternParent, dts + "Executables");
                        XElement elemPatternParentNextSqlChild = getChildElementUntilCertain(elemPatternParentNextSql, dts + "ObjectData");
                        XElement elemPatternParentNextSqlExec = elemPatternParentNextSqlChild?.Element(sql + "SqlTaskData");

                        string sourceVariableFull = elemContainsSourceVariable?.Attribute(dts + "VariableName")?.Value;
                        string sourceVariable = sourceVariableFull?.Substring(sourceVariableFull.IndexOf(':') + 2);
                        sourceVariable = sourceVariable?.Substring(0, sourceVariable.IndexOf('_'));

                        XElement elemSourceDatabase = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_DB_Name").FirstOrDefault();
                        XElement elemSourceLinkedSever = doc.Descendants().Where(x => x.Attribute(dts + "ObjectName")?.Value == sourceVariable + "_Link_Name").FirstOrDefault();

                        string sourceDB = elemSourceDatabase?.Value;
                        string sourceLink = elemSourceLinkedSever?.Value;

                        string sqlCommandFull = elemPatternParentNextSqlExec?.Attribute(sql + "SqlStatementSource")?.Value;
                        if (sqlCommandFull != null)
                        {
                            if (sqlCommandFull.Contains("usp_ImportDataFromSQLServer"))
                            {
                                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
                                sqlCommandFull = sqlCommandFull.Replace("@Link_Name", "'" + sourceLink + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@DB_Name", "'" + sourceDB + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableNameSource", "'" + ReturnListValueCertainIndex(parameters, 0) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableNameDestination", "'" + ReturnListValueCertainIndex(parameters, 1) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableIncludedColumns", "'" + ReturnListValueCertainIndex(parameters, 2) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableExcludedColumns", "'" + ReturnListValueCertainIndex(parameters, 3) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableDistinct", ReturnListValueCertainIndex(parameters, 4));
                            }
                            else if (sqlCommandFull.Contains("usp_LoadFactPart"))
                            {
                                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
                                sqlCommandFull = sqlCommandFull.Replace("@sSourceViewName", "'" + ReturnListValueCertainIndex(parameters, 0) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@sFactTableName", "'" + ReturnListValueCertainIndex(parameters, 1) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@sPartColumnDate", "'" + ReturnListValueCertainIndex(parameters, 2) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@sExcludedColumns", "'" + ReturnListValueCertainIndex(parameters, 3) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@nUseTempTable", ReturnListValueCertainIndex(parameters, 4));
                                sqlCommandFull = sqlCommandFull.Replace("@nMode", ReturnListValueCertainIndex(parameters, 5));
                                sqlCommandFull = sqlCommandFull.Replace("@sTablePartition", "'" + ReturnListValueCertainIndex(parameters, 6) + "'");
                            }
                            else if (sqlCommandFull.Contains("usp_LoadHistoryDim"))
                            {
                                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
                                sqlCommandFull = sqlCommandFull.Replace("@ViewNameHistory", "'" + ReturnListValueCertainIndex(parameters, 0) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableNameHistory", "'" + ReturnListValueCertainIndex(parameters, 1) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@NaturalColumns", "'" + ReturnListValueCertainIndex(parameters, 2) + "'");
                            }
                            else if (sqlCommandFull.Contains("usp_LoadHistoryPart"))
                            {
                                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
                                sqlCommandFull = sqlCommandFull.Replace("@ViewNameHistory", "'" + ReturnListValueCertainIndex(parameters, 0) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@TableNameHistory", "'" + ReturnListValueCertainIndex(parameters, 1) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@WhereColumns", "'" + ReturnListValueCertainIndex(parameters, 2) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@HorizonLoadData", "'" + ReturnListValueCertainIndex(parameters, 3) + "'");
                            }
                            else if (sqlCommandFull.Contains("usp_LoadOtherDIM"))
                            {
                                sqlCommandFull = sqlCommandFull.Replace("@Batch", "-69");
                                sqlCommandFull = sqlCommandFull.Replace("@DIM_SourceViewName", "'" + ReturnListValueCertainIndex(parameters, 0) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@DIM_DimensionTableName", "'" + ReturnListValueCertainIndex(parameters, 1) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@DIM_MergeOnColumns", "'" + ReturnListValueCertainIndex(parameters, 2) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@DIM_MergeExcludedColumns", "'" + ReturnListValueCertainIndex(parameters, 3) + "'");
                                sqlCommandFull = sqlCommandFull.Replace("@DIM_Mode", "'" + ReturnListValueCertainIndex(parameters, 4) + "'");
                            }

                        }

                        results.Add(new ResultSet(ReturnListValueCertainIndex(parameters, 1), ReturnListValueCertainIndex(parameters, 0), FullPath, sourceVariable, sourceLink, sourceDB, sqlCommandFull));

                    }
 
                    return results.Distinct();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString() + "\n" + ex.Message);
                    return null;
                }
            }
            else
            {
                return null;
            }

    }

        //public async Task GetSourceTableAsync()
        //{
        //    await Task.Run(() => GetSourceTable());
        //}
    }
}
