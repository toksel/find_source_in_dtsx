﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace FindSourceInDtsx
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (Directory.Exists(@"C:\ETL_Solution\ETL_Solution"))
            {
                txtbox_folder_path.Text = @"C:\ETL_Solution\ETL_Solution";
            }
            else
            {
                txtbox_folder_path.Text = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            }

            txtbox_table_name.Text = "2. Copy/type table name (pattern) you want to find source";

        }

        private void btn_folder_path_Button_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new CommonOpenFileDialog())
            {
                var initialFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                var startUpPath = initialFolder.Substring(0, initialFolder.LastIndexOf('\\'));
                dialog.Title = "Укажите папку";
                dialog.IsFolderPicker = true;
                dialog.InitialDirectory = startUpPath;
                dialog.AddToMostRecentlyUsedList = false;
                dialog.AllowNonFileSystemItems = false;
                dialog.DefaultDirectory = startUpPath;
                dialog.EnsurePathExists = true;
                dialog.Multiselect = false;
                dialog.ShowPlacesList = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var folder = dialog.FileName;
                    txtbox_folder_path.Text = folder;
                }

            }
        }

        private void btn_start_Button_Click(object sender, RoutedEventArgs e)
        {
            txtbox_result.Clear();
            if (string.IsNullOrEmpty(txtbox_table_name.Text))
            {
                txtbox_result.Text += "Забыл ввести название таблицы\n";
                return;
            }

            if (Directory.Exists(txtbox_folder_path.Text))
            {
                var resultSet = new List<ResultSet>();
                foreach (string path in ReturnPath(txtbox_folder_path.Text, "*.dtsx"))
                {
                    var file = new DTSX(path, txtbox_table_name.Text);
                    IEnumerable<ResultSet> resultList = file.GetSourceTable();
                    if (resultList != null)
                    {
                        foreach (var result in resultList)
                        {
                            if (result != null)
                            {
                                resultSet.Add(result);
                            }
                        }
                    }
                                       
                }
                var sortedResultSet = resultSet?.OrderByDescending(x => x.CountNonEmptyField);
                foreach (var result in sortedResultSet)
                {
                    txtbox_result.Text += result.ToString();
                }
            }
            else
            {
                txtbox_result.Text += "Неверная директория\n";
            }

            if (string.IsNullOrWhiteSpace(txtbox_result.Text))
            {
                txtbox_result.Text = "No matches found";
            }

        }

        // ищем файлы в выбранной папке по паттерну
        public static List<string> ReturnPath(string directory, string pattern)
        {
            List<string> outList = new List<string>();
            foreach (var file in Directory.GetFiles(directory, pattern, SearchOption.AllDirectories))
            {
                outList.Add(file);
            }
            return outList;
        }

        private void txtbox_table_name_GotMouseCapture(object sender, MouseEventArgs e)
        {
            txtbox_table_name.Clear();
        }
    }
}
